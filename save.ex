defmodule Save do
  def harvester_rescue(data) do
    worm_distance = distance(data[:harvester], List.first(data[:worm]))
    worm_eta = worm_distance / List.last(data[:worm])
    carryall_distance = distance(data[:harvester], List.first(data[:carryall]))
    carryall_eta = carryall_distance / List.last(data[:carryall])
    carryall_process_time = 1
    IO.inspect(data)
    result(worm_eta, carryall_eta + carryall_process_time)
  end

  defp distance([x1, y1], [x2, y2]) do
    :math.sqrt(square(x1 - x2) + square(y1 - y2))
  end

  defp square(x), do: x * x

  defp result(x, y) when x > y, do: "The spice must flow! Rescue the harvester!"
  defp result(x, y) when x <= y, do: "Damn the spice! I'll rescue the miners!"

end

hr = &Save.harvester_rescue/1
IO.inspect(hr.([harvester: [345, 600], worm: [[200, 100], 25], carryall: [[350, 200], 32]]))
IO.inspect(hr.([harvester: [200,400], worm: [[200,0],40], carryall: [[500,100],45]]))
IO.inspect(hr.([harvester: [850,125], worm: [[80,650],20], carryall: [[80,600],20]]))
IO.inspect(hr.([harvester: [0,320], worm: [[250,680],42], carryall: [[550,790],58]]))
IO.inspect(hr.([harvester: [0,0], worm: [[0,600],50], carryall: [[0,880],80]]))